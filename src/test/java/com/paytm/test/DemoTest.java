package com.paytm.test;

import static io.restassured.RestAssured.given;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.paytm.listeners.FrameworkAssert;
import com.paytm.pojo.response.Access_Token;
import com.paytm.pojo.response.AuthCode;
import com.paytm.pojo.response.GeneralResponse;
import com.paytm.pojo.response.GridLayout;
import com.paytm.pojo.response.GridResponse;
import com.paytm.requestbodyproviders.CategoryJsonBodyProvider;
import com.paytm.rest.requestresponse.AccesssTokenRequest;
import com.paytm.rest.requestresponse.AuthCodeRequest;
import com.paytm.rest.requestresponse.GeneralRequest;
import com.paytm.rest.requestresponse.GridRequest;
import com.paytm.rest.requestresponse.RequestType;
import com.paytm.utils.DBFunctions;
import com.paytm.utils.JsonHandler;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class DemoTest {
	DBFunctions dbfs;
	PrintStream logStream;
	
	@BeforeTest
	public void setup() throws FileNotFoundException {
		RestAssured.baseURI="https://catalog-staging.paytm.com";
		dbfs = new DBFunctions();
		/*
		 * logStream = new PrintStream( GeneralUtils.createNewFileWithTimeStamp(
		 * "./logs", "log", "txt")); 
		 * RestAssured.config = config().logConfig(new
		 * LogConfig().defaultStream(logStream));
		 */
		
	}
	
	@Test(enabled=false)
	public void test() throws JsonParseException, JsonMappingException, IOException {
		Response response = given()
			.when()
			.get("/printers-glpid-28996?channel=web&child_site_id=6&site_id=2&version=2&items_per_page=32&pin_code=110091")
			.then()
			.statusCode(200)
			.extract().response();
		
		System.out.println(response.asString());
	//	RestAssured.config().getJsonConfig()
		//GridResponse grid2 = (GridResponse)response.as(GridResponse.class);
		//System.out.println("Add to cart for grid : "+grid2.getAdd_to_cart());
		GridResponse grid = (GridResponse)JsonHandler.getObjectFromJsonString(response.asString(), GridResponse.class) ;
		GridLayout product1 = grid.getGrid_layout().get(0);
		System.out.println(product1.getCategory_id());
		
		GridResponse grid2 = JsonHandler.getObjectFromResponse(response, GridResponse.class);
		GridLayout product2 = grid.getGrid_layout().get(1);
		System.out.println(product2.getActual_price());
	}
	
	@Test (enabled=false)
	public void getAuthToken() throws IOException {
		String code = given()
				.when()
				.get("https://persona-staging.paytm.com/oauth2/authorize?"
						+ "username=chirag.bansal@paytmmall.com"
						+ "&"
						+ "password=paytm@567"
						+ "&"
						+ "state=a1b2c3d4"
						+ "&"
						+ "notredirect=true"
						+ "&"
						+ "client_id=merchant-integration"
						+ "&"
						+ "response_type=code'")
				.then()
				.statusCode(200)
				.extract()
				.path("code");
		
		
		String access_token = given()
				.when()
				.get("https://persona-staging.paytm.com/oauth2/token?"
						+ "code"+"="+code
						+ "&"
						+ "client_id=merchant-integration"
						+ "&"
						+ "client_secret=1f9009680f0b950d502b7b59e6eb3e312344d137"
						+ "&"
						+ "grant_type=authorization_code"
						+ "&"
						+ "state=a1b2c3d4")
				.then()
				.statusCode(200)
				.extract()
				.path("access_token");
		
		System.out.println("Access Token is : "+ access_token);
		
		//String access_token = AuthTokenProvider.getAccessToken();
		
		String categoryResponse = given()
		.queryParams("client", "web")
		.queryParam("authtoken", access_token)
		.formParam("vertical_id", 2)
		.formParam("data", new CategoryJsonBodyProvider().getJson())
		.when()
		.post("https://catalogadmin-staging.paytm.com/dev1/v1/category")
		.then()
		.extract()
		.response().asString();
		
		System.out.println(categoryResponse);
	}
	
	@Test(enabled=false)
	public void test_disabledGrid() throws Exception {
		int newCategoryID = 163267; //ResourceCreator.createNewCategory();
		String query_makeCategoryVisible = "UPDATE catalog_category"+" "+
											"SET visibility=1,status=1"+" "+
											"WHERE id="+newCategoryID+
											";";
		
		String query_makeCategoryInvisible = "UPDATE catalog_category"+" "+
				"SET visibility=0,status=0"+" "+
				"WHERE id="+newCategoryID+
				";";
		
		String query_getSeoURL = "SELECT seo_url"+" "+
								 "FROM catalog_category"+" "+
								 "WHERE id="+newCategoryID+";";
		
		String seoURL = dbfs.SelectQuery(1, query_getSeoURL, "mktplace_catalog");
		System.out.println(seoURL);
		dbfs.insertUpdateRecord(query_makeCategoryVisible, "mktplace_catalog");
		
		Thread.sleep(15000);
		
		String resourceURL = "/"+seoURL+"-glpid-"+newCategoryID;
//		Response response = given().log().all()
//		.spec(SpecProvider.reqSpecGrid)
//		.when()
//		.get(resourceURL)
//		.then().log().all()
//		.extract().response();
//		
//		GridResponse gResponse = JsonHandler.getObjectFromResponse(response, GridResponse.class);
	
		GridResponse gResponse = (GridResponse) new GridRequest("/"+seoURL+"-glpid-"+newCategoryID).call();
		int gridType = gResponse.getGrid_type();
		FrameworkAssert.assertEquals(gridType, 1, 
				"Grid Type should be 1 but actual is "+gridType);
		
		new GridRequest("/test-glpid-1100").call();
		
		new GridRequest("/test-glpid-2299").call();
		
		dbfs.insertUpdateRecord(query_makeCategoryInvisible, "mktplace_catalog");
		Thread.sleep(15000);
//		Response response = given().log().all()
//				.spec(SpecProvider.reqSpecGrid)
//				.when()
//				.get(resourceURL)
//				.then().log().all()
//				.extract().response();
//		System.out.println(response.asString());
//		FrameworkAssert.assertEquals(response.getStatusCode(), 404, 
//				"Grid should give 404 becuase category status and visibility is 0");
		
		gResponse =  (GridResponse) new GridRequest("/"+seoURL+"-glpid-"+newCategoryID)
				.setQueryParameter("items_per_page", "3").call();
		System.out.println(gResponse);
	}
	
	@Test(enabled=false)
	public void test11111() throws JsonParseException, JsonMappingException, IOException {
		//GridResponse gr = (GridResponse) new GridRequest("/test-glpid-22233").call();
		AuthCode code = (AuthCode) new AuthCodeRequest().call();
		System.out.println("Auth code is : "+code.getCode());
		Access_Token token = (Access_Token) new AccesssTokenRequest(code.getCode()).call();
		System.out.println("Access token : "+token.getAccess_token());
	}
	
	@Test
	public void test22() throws JsonParseException, JsonMappingException, IOException {
		
		given().log().all().when()
		.get("https://reqres.in/api/users?id=2")
		.then().extract().response().asString();
		
		Response res =  new GeneralRequest(
				RequestType.GET, "https://reqres.in/api/users?id=2").
				getResponse();
		System.out.println(res.asString());
		
		Response res3 =  new GeneralRequest(
				RequestType.GET, "https://reqres.in/api/users/abcd/ag?id=7").
				getResponse();
		
		System.out.println(res3.asString());
		
		GeneralResponse res2 = (GeneralResponse)new GeneralRequest(RequestType.POST, "https://reqres.in/api/users")
				.setBody("{\n" + 
						"    \"name\": \"ChiragBansal\",\n" + 
						"    \"job\": \"SDET\"\n" + 
						"}").call();
		System.out.println(res2.getRestResponse().asString());
	
	}
	
	@AfterTest
	public void cleanUP() {
		dbfs.closeAllConnections();
	}
}
