package com.paytm.test;

import com.paytm.resourcecreators.ResourceCreator;

public class Test {
	
	public static void main(String[] args) throws Exception {
		int categoryId = ResourceCreator.createNewCategory();
		System.out.println("Category ID created - "+categoryId);
		//String categoryId = "163264";
		int productId = ResourceCreator.createNewProduct(categoryId,1).get(0);
		System.out.println("Product created with PID - "+productId);
	}
}
