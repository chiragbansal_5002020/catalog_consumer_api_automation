package com.paytm.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.paytm.listeners.FrameworkAssert;
import com.paytm.pojo.response.GridLayout;
import com.paytm.pojo.response.GridResponse;
import com.paytm.pojo.tests.Actions;
import com.paytm.pojo.tests.CategoryInfoString;
import com.paytm.pojo.tests.ProductInfoString;
import com.paytm.resourcecreators.ResourceCreator;
import com.paytm.rest.requestresponse.GridRequest;
import com.paytm.utils.ConfigReader;
import com.paytm.utils.DBFunctions;
import com.paytm.utils.GeneralUtils;
import com.paytm.utils.JsonHandler;
import com.paytm.utils.ResponseCodeValidator;

import io.restassured.RestAssured;

public class GridTestCases {
	int categoryIDToTest;
	String gridURL;
	DBFunctions dbfs;

	@BeforeTest
	public void setup() throws Exception {
		dbfs = new DBFunctions();
		categoryIDToTest= 0; //ResourceCreator.createNewCategory();
		if(categoryIDToTest==0) {
			categoryIDToTest = Integer.parseInt(ConfigReader.getValue("backupCategoryID"));
			//ResourceCreator.createNewProduct(categoryIDToTest,5);
		}else {
			List<Integer> newProductsList = ResourceCreator.createNewProduct(categoryIDToTest,15);
			System.out.println("New Product IDs : "+newProductsList);
		}
		String query_getSeoURL = "SELECT seo_url"+" "+
				"FROM catalog_category"+" "+
				"WHERE id="+categoryIDToTest+";";

		String seoURL = dbfs.SelectQuery(1, query_getSeoURL, "mktplace_catalog");
		System.out.println("SEO URL for category - "+seoURL);
		gridURL = "/"+seoURL+"-glpid-"+categoryIDToTest;
		RestAssured.baseURI="https://catalog-staging.paytm.com";
	}
	

	@Test(description="Testing Grid Enabled / Disabled",enabled=false)
	public void test_gridEnableDisable() throws Exception {

		String query_makeCategoryVisible = "UPDATE catalog_category"+" "+
				"SET visibility=1,status=1"+" "+
				"WHERE id="+categoryIDToTest+
				";";

		String query_makeCategoryInvisible = "UPDATE catalog_category"+" "+
				"SET visibility=0,status=0"+" "+
				"WHERE id="+categoryIDToTest+
				";";



		dbfs.insertUpdateRecord(query_makeCategoryVisible, "mktplace_catalog");

		Thread.sleep(20000);

		GridResponse gResponse = (GridResponse) new GridRequest(gridURL).call();
		int gridType = gResponse.getGrid_type();

		FrameworkAssert.assertEquals(gridType, 1, 
				"Grid Type should be 1 and actual Grid Type is "+gridType);

		dbfs.insertUpdateRecord(query_makeCategoryInvisible, "mktplace_catalog");
		Thread.sleep(20000);

		gResponse =  (GridResponse) new GridRequest(gridURL).call();
		System.out.println(gResponse);

	}

	@Test(description="Testiing for grid price filter",
			enabled=false)
	public void test_gridPriceFilter() throws JsonParseException, JsonMappingException, IOException {
		GridRequest request = new GridRequest(gridURL);
		GridResponse gResponse = (GridResponse) request.call();
		
		FrameworkAssert.assertEquals(request.getStatus(), 200);
	
		
		List<Double> prices = new ArrayList<>();
		double minAmount = Double.parseDouble(gResponse.getFilters().get(0).getValues().get(0).getMin());
		double maxAmount = Double.parseDouble(gResponse.getFilters().get(0).getValues().get(0).getMax());

		System.out.println("Minimum Price in prie filter  - "+minAmount);
		System.out.println("Maximum Price in price filter - "+maxAmount);

		System.out.println("Testiing with minimum price filter -- ");
		gResponse = (GridResponse) new GridRequest(gridURL)
				.setQueryParameter("price", minAmount+","+minAmount)
				.call();
		FrameworkAssert.assertEquals(request.getStatus(), 200);
		

		for(GridLayout layout : gResponse.getGrid_layout()) {
			prices.add(layout.getOffer_price());
		}
		
		System.out.println("Number of products after applying filter -- "+gResponse.getTotalCount());
		System.out.println("Product Prices after applying price filter -- "+prices);

		FrameworkAssert.assertTrue(
				GeneralUtils.isEveryValueInListInRange(prices, minAmount, minAmount),
				"Products on grid should be within price range !!");


		System.out.println("Testing with max amount in filter --- ");
		prices.clear();
		
		gResponse = (GridResponse) request.setQueryParameter("price", maxAmount+","+maxAmount)
					.call();
		FrameworkAssert.assertEquals(request.getStatus(), 200);

		for(GridLayout layout : gResponse.getGrid_layout()) {
			prices.add(layout.getOffer_price());
		}
		
		System.out.println("Number of products after applying filter -- "+gResponse.getTotalCount());
		System.out.println("Product Prices after applying price filter -- "+prices);

		FrameworkAssert.assertTrue(
				GeneralUtils.isEveryValueInListInRange(prices, maxAmount, maxAmount),
				"Products on grid should be within price range !!");


		System.out.println("Setting random price range !");
		prices.clear();
		boolean isProductFoundOnGrid=false;
		int retryCount = 5;
		double randomMinPrice;
		double randomMaxPrice;
		do {
			randomMinPrice = GeneralUtils.randomNumberGenerator(minAmount, (minAmount+maxAmount)/2);
			randomMaxPrice = GeneralUtils.randomNumberGenerator((minAmount+maxAmount)/2, maxAmount);
			System.out.println("Random Minimum price --- "+randomMinPrice);
			System.out.println("Random Maximum price --- "+randomMaxPrice);
			gResponse = (GridResponse) request
					.setQueryParameter("price", randomMinPrice+","+randomMaxPrice)
					.call();
			FrameworkAssert.assertEquals(request.getStatus(), 200);
			if(gResponse.getTotalCount()>0) {
				isProductFoundOnGrid=true;
				for(GridLayout layout : gResponse.getGrid_layout()) {
					prices.add(layout.getOffer_price());
				}
				System.out.println("Product Prices after applying price filter -- "+prices);
				
				FrameworkAssert.assertTrue(
						GeneralUtils.isEveryValueInListInRange(prices, randomMinPrice, randomMaxPrice),
						"Products on grid should be within price range !!");
			}
			retryCount--;
		}while(isProductFoundOnGrid!=true && retryCount>0);
	}


	@Test(description="Add to Cart Test", enabled=false)
	public void test_addToCart() throws Exception {
		String query_getInfo = "SELECT info"
				+" FROM catalog_category"
				+" WHERE id="+categoryIDToTest
				+" ;";
		String infoJson = dbfs.SelectQuery(1, query_getInfo, "mktplace_catalog");
		System.out.println(infoJson);
		
		CategoryInfoString info = (CategoryInfoString) JsonHandler.getObjectFromJsonString(infoJson, CategoryInfoString.class);
		Actions action = info.getActions();
		if(action == null) {
			action = new Actions();
		}
		action.setAdd_to_cart(1);
		info.setActions(action);
		String updatedCategoryInfo = JsonHandler.getJsonStringFromObject(info);
		System.out.println("Updated Info String -- "+updatedCategoryInfo);
		String query_enableAddToCartForCategory = "UPDATE catalog_category"
				+" SET info='"+updatedCategoryInfo+"'"
				+" WHERE id="+categoryIDToTest
				+";";
		dbfs.insertUpdateRecord(query_enableAddToCartForCategory , "mktplace_catalog");
		
		Thread.sleep(20000);
		GridRequest request = new GridRequest(gridURL);
		GridResponse response = (GridResponse)request.call();
		FrameworkAssert.assertEquals(request.getStatus(), 200);
		System.out.println("Add to Cart Value : "+response.getAdd_to_cart());
		FrameworkAssert.assertEquals( response.getAdd_to_cart(),1);
		
		info.getActions().setAdd_to_cart(0);
		updatedCategoryInfo = JsonHandler.getJsonStringFromObject(info);
		String query_disableAddToCartForCategory = "UPDATE catalog_category"
				+" SET info='"+updatedCategoryInfo+"'"
				+" WHERE id="+categoryIDToTest
				+";";
		dbfs.insertUpdateRecord(query_disableAddToCartForCategory, "mktplace_catalog");
		
		Thread.sleep(20000);
		response = (GridResponse)request.call();
		FrameworkAssert.assertEquals(request.getStatus(), 200);
		System.out.println("Add to cart value : "+response.getAdd_to_cart());
		FrameworkAssert.assertEquals(response.getAdd_to_cart(), 0);
	
	}
	
	@Test(description="Test Discoverability", enabled=false)
	public void test_discoverability() throws Exception {
		String query_getProductsOfCategory = "SELECT id "
				+"FROM catalog_product "
				+"WHERE category_id='"+categoryIDToTest+"';";
		String[] productIDs = dbfs.SelectQuery(1, query_getProductsOfCategory, "mktplace_catalog")
				.split(";;;");
		System.out.println("Product IDs : ");
		for(String id : productIDs)
			System.out.print(id+", ");
		String productID = productIDs[0];
		System.out.println("Changing discoverability of product id : "+productID);
		
		String query_getInfo = "SELECT info"
				+" FROM catalog_product"
				+" WHERE id="+productID
				+" ;";
		
		String infoString = dbfs.SelectQuery(1, query_getInfo, "mktplace_catalog");
		
		ProductInfoString productInfo = (ProductInfoString) JsonHandler.getObjectFromJsonString(infoString, ProductInfoString.class);
		productInfo.setDiscoverability(new int[]{2});
		
		String query_updateProductInfoString = "UPDATE catalog_product"
										+" SET info='"+JsonHandler.getJsonStringFromObject(productInfo)+"'"
										+" WHERE id='"+productID+"'"
										+";";
		
		dbfs.insertUpdateRecord(query_updateProductInfoString, "mktplace_catalog");
		Thread.sleep(20000);
		GridRequest request = new GridRequest(gridURL);
		request.setQueryParameter("discoverability", "offline");
		GridResponse response = (GridResponse) request.call();
		List<GridLayout> layouts = response.getGrid_layout();
		boolean isProductFound=false;
		for(GridLayout layout: layouts) {
			if(layout.getProduct_id()==Integer.parseInt(productID))
				isProductFound=true;
		}
		Assert.assertEquals(request.getStatus(), 200);
		FrameworkAssert.assertTrue(isProductFound, "After changing discoverability offline, product should visible !");
		
		
		//Testing for b2b discoverability
		productInfo.setDiscoverability(new int[] {3});
		dbfs.insertUpdateRecord(query_updateProductInfoString, "mktplace_catalog");
		Thread.sleep(20000);
		request = new GridRequest(gridURL);
		request.setQueryParameter("discoverability", "b2b");
		response = (GridResponse) request.call();
		layouts = response.getGrid_layout();
		isProductFound=false;
		for(GridLayout layout: layouts) {
			if(layout.getProduct_id()==Integer.parseInt(productID))
				isProductFound=true;
		}
		Assert.assertEquals(request.getStatus(), 200);
		FrameworkAssert.assertTrue(isProductFound, "After changing discoverability b2b, product should visible !");
		
		//Testing for MyStrore discoverability
		productInfo.setDiscoverability(new int[] {4});
		dbfs.insertUpdateRecord(query_updateProductInfoString, "mktplace_catalog");
		Thread.sleep(20000);
		request = new GridRequest(gridURL);
		request.setQueryParameter("discoverability", "mystore");
		response = (GridResponse) request.call();
		layouts = response.getGrid_layout();
		isProductFound=false;
		for(GridLayout layout: layouts) {
			if(layout.getProduct_id()==Integer.parseInt(productID))
				isProductFound=true;
		}
		Assert.assertEquals(request.getStatus(), 200);
		FrameworkAssert.assertTrue(isProductFound, "After changing discoverability mystore, product should visible !");
		
	}
	
	
	@Test(description = "Test serviceability", enabled = true)
	public void test_serviceability() throws Exception {
		
		String query_getInfo = "SELECT info"
				+" FROM catalog_category"
				+" WHERE id="+categoryIDToTest
				+" ;";
		
		String infoString = dbfs.SelectQuery(1, query_getInfo, "mktplace_catalog");
		CategoryInfoString categoryInfo = (CategoryInfoString) JsonHandler.getObjectFromJsonString(infoString, CategoryInfoString.class);
		categoryInfo.setServiceability_enabled(1);
		
		String query_updateCategoryInfoString = "UPDATE catalog_category"
				+" SET info='"+JsonHandler.getJsonStringFromObject(categoryInfo)+"'"
				+" WHERE id='"+categoryIDToTest+"'"
				+";";
		
		
		dbfs.insertUpdateRecord(query_updateCategoryInfoString, "mktplace_catalog");
		Thread.sleep(10000);
		GridRequest request = new GridRequest(gridURL);
		GridResponse response = (GridResponse) request.call();
		
		FrameworkAssert.assertEquals(response.getServiceability_enabled(),1, 
				"Value of serviceability_enabled should be 1 but found "+response.getServiceability_enabled());
		
		
		categoryInfo.setServiceability_enabled(0);
		
		query_updateCategoryInfoString = "UPDATE catalog_category"
				+" SET info='"+JsonHandler.getJsonStringFromObject(categoryInfo)+"'"
				+" WHERE id='"+categoryIDToTest+"'"
				+";";
		
		
		dbfs.insertUpdateRecord(query_updateCategoryInfoString, "mktplace_catalog");
		Thread.sleep(10000);
		request = new GridRequest(gridURL);
		response = (GridResponse) request.call();
		
		FrameworkAssert.assertEquals(response.getServiceability_enabled(),0, 
				"Value of serviceability_enabled should be 0 but found "+response.getServiceability_enabled());
		
		
		
	}
	
	
	@Test(description="Test URLs on Grid Page", enabled=false)
	public void test_allResponseLink() throws Exception {
		SoftAssert sa = new SoftAssert();
		GridRequest request = new GridRequest(gridURL);
		GridResponse response = (GridResponse) request.call();
		List<GridLayout> gridLayoutList = response.getGrid_layout();
		
		List<ResponseCodeValidator> threadsList = new ArrayList<ResponseCodeValidator>();
		for(GridLayout gridLayout : gridLayoutList) {
			ResponseCodeValidator imageUrlValidator = new ResponseCodeValidator(gridLayout.getImage_url());
			threadsList.add(imageUrlValidator);
			ResponseCodeValidator seoUrlValidator = new ResponseCodeValidator(gridLayout.getSeourl());
			threadsList.add(seoUrlValidator);
			ResponseCodeValidator UrlValidator = new ResponseCodeValidator(gridLayout.getUrl());
			threadsList.add(UrlValidator);
			ResponseCodeValidator NewUrlValidator = new ResponseCodeValidator(gridLayout.getNewurl());
			threadsList.add(NewUrlValidator);
			imageUrlValidator.start();
			seoUrlValidator.start();
			UrlValidator.start();
			NewUrlValidator.start();
		}
		
		for(ResponseCodeValidator validator : threadsList) {
			try {
				validator.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		for(ResponseCodeValidator validator : threadsList) {
			System.out.println(validator.url+"\n"+validator.getResponseCode());
			 sa.assertEquals(validator.getResponseCode(), 200,
					  "Response Code is not 200 for url - "+ validator.url);
		}
		
		sa.assertAll();
	}
	
	@Test(description="Test Product Count and Pagination on Grid", enabled=true)
	public void test_productCountAndPagination() throws Exception {
		
		GridRequest request = new GridRequest(gridURL);
		request.setQueryParameter("items_per_page", Integer.toString(2));
		GridResponse response = (GridResponse) request.call();
		
		List<GridLayout> gridLayoutList = response.getGrid_layout();
		Set<Integer> productIDsSetFirstPage = gridLayoutList.stream()
				.map(g->g.getProduct_id()).collect(Collectors.toSet());
		
		Assert.assertEquals(productIDsSetFirstPage.size(),2,
				"Expected 2 different products on first page but found "+productIDsSetFirstPage.size()+" products."+"\n"+
						"Product IDs " + productIDsSetFirstPage);
		
		request.setQueryParameter("page", "2");
		request.setQueryParameter("page_count","2");
		response = (GridResponse)request.call();
		gridLayoutList = response.getGrid_layout();
		
		Set<Integer> productIDsSetSecondPage = gridLayoutList.stream()
				.map(g->g.getProduct_id()).collect(Collectors.toSet());
		
		Assert.assertEquals(productIDsSetSecondPage.size(),2,
				"Expected 2 products on second page but found "+productIDsSetSecondPage.size()+" products."+"\n"+
						"Product IDs - "+productIDsSetSecondPage);
		
		Assert.assertTrue(GeneralUtils.areTwoSetsDisjoint(productIDsSetFirstPage, productIDsSetSecondPage),
				"Products are expected to be different on page 1 and page 2. But some products are not different!");	
	}
	
	@AfterTest
	public void tearDown() {
		
	}
}
