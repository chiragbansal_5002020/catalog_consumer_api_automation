package com.paytm.utils;

import java.io.IOException;

public class ResponseCodeValidator extends Thread{
		public String url;
		private int responseCode;
		public ResponseCodeValidator(String url) {
			this.url = url;
		}
		public int getResponseCode() {
			return this.responseCode;
		}
		@Override
		public void run() {
			try {
				this.responseCode = GeneralUtils.getResponseCodeForRequest(this.url);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
}
