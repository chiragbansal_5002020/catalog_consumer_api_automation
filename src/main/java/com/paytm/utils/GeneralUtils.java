package com.paytm.utils;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.testng.reporters.Files;

public class GeneralUtils {
	
	public static String readFileContent(String filePath) throws IOException {
		return Files.readFile(new File(filePath));
	}

	
	public static int randomNumberGenerator(int min, int max) {
		Random randomGeneratorEngine = new Random();
		return randomGeneratorEngine.nextInt(max+1-min)+min;
	}
	
	public static double randomNumberGenerator(double min, double max) {
		double val = min+Math.random()*(max-min);
		val = val*100;
		val = Math.round(val);
		val = val/100;
		return val;
	}
	
	public static File createNewFileWithTimeStamp(String directoryPath, String fileName, String extension) {
		File directory = new File(directoryPath);
		if(!directory.exists())
			directory.mkdirs();
		if(extension.startsWith(".")){
			extension=extension.substring(1);
		}
		return new File(directory+"/"+fileName+"_"+System.currentTimeMillis()+"."+extension);
	}
	
	@SuppressWarnings("unchecked")
	public static <O extends Comparable> boolean isEveryValueInListInRange(List<O> list, O minVal, O maxVal){
		boolean areAllValuesInRange = true;
		for(O current : list) {
			if(current.compareTo(minVal)>=0 && current.compareTo(maxVal)<=0) {
				continue;
			}
			else {
				areAllValuesInRange = false;
				break;
			}
		}
		return areAllValuesInRange;
	}
	
	
	public static int getResponseCodeForRequest(String uri) throws IOException  {
		int responseCode =  Integer.MIN_VALUE; 
		try {
			URL url = new URL(uri);
			HttpURLConnection connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod("GET");
			connection.setConnectTimeout(5000);
			connection.setReadTimeout(5000);
			connection.connect();
			responseCode = connection.getResponseCode();
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return responseCode;
	}
	
	
	public static boolean areTwoSetsDisjoint(Set<Integer> set1, Set<Integer> set2)  {
	    { 
	    	System.out.println("Set 1 - "+set1);
	    	System.out.println("Set 2 - "+set2);
	        int i=0,j=0; 
	        ArrayList<Integer> set1AsList = new ArrayList<Integer>(set1);
	        ArrayList<Integer> set2AsList = new ArrayList<Integer>(set2);
	        Collections.sort(set1AsList);
	        Collections.sort(set2AsList);
	          
	        while(i<set1AsList.size() && j<set1AsList.size()) 
	        { 
	            if(set1AsList.get(i)<set2AsList.get(j)) 
	                i++; 
	            else if(set1AsList.get(i)>set2AsList.get(j)) 
	                j++; 
	            else 
	                return false; 
	              
	        } 
	        return true; 
	    } 
}
	
	public static void main(String[] args) {
		List<Double> list = Arrays.asList(65399.0, 43500.0, 99500.0, 98500.0, 41000.0);
		System.out.println(isEveryValueInListInRange(list, 40936.65945464068, 133640.260574349));
	}
	
	
}
