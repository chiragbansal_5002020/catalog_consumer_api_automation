package com.paytm.utils;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.restassured.response.Response;

public class JsonHandler {
	static ObjectMapper mapper;
	static {
		if(mapper==null)
			mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}
	
	public static String getJsonStringFromObject(Object object) throws JsonProcessingException {
		return mapper.writeValueAsString(object); 
	}
	
	public static Object getObjectFromJsonString(String jsonString, Class<?> className) throws JsonParseException, JsonMappingException, IOException {
		return mapper.readValue(jsonString, className);
		
	}
	
	public static <T> T getObjectFromResponse(Response response, Class<?> className) throws JsonParseException, JsonMappingException, IOException{
		return (T)mapper.readValue(response.asString(), className);
		
	}
	
}
