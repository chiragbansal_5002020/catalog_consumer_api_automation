package com.paytm.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.RandomStringUtils;


public class generic_methods 
{

	public String getExceptionInfo(Exception e)	
	{
		String exceptionInfo = null;
		Integer lineNo = null;
		String methodName = null;
		String className = null;
		String exceptionReason = null;
		StackTraceElement ste = null;

		StackTraceElement[] stackInfo = e.getStackTrace();

		for (int i = 0; i < stackInfo.length; i++) 
		{
			ste = stackInfo[i];

			if(ste != null)
			{
				if(ste.toString().trim().toUpperCase().contains("src".toUpperCase()) )
				{
					lineNo = ste.getLineNumber();
					methodName =ste.getMethodName();
					className = ste.getClassName();
					exceptionReason = e.getClass().toString();

					//if(exceptionReason != null && className != null && methodName != null && lineNo != null)
					exceptionInfo = "Exception Message : "+e.getMessage()+" ["+exceptionReason+"] at Line No. '"+lineNo+"' in Method '"+methodName+"' of Class '"+className+"'";
					break;
				}
			}	 
		}

		return exceptionInfo;
	}

	public  String generateRandomString(int length)
	{
		String random="";
		random=RandomStringUtils.randomAlphabetic(length).toLowerCase();
		return random;
	}

	public int generateRandomNumber(int min,int max)
	{
		int number;
		//number=(int) (Math.random()*(max-min)+1);
		number=(int) Math.floor(Math.random() * ((max-min)+1) + min);
		return number;

	}

	public  File getLatestFilefromDir(String dirPath)
	{
		File dir = new File(dirPath);
		File[] files = dir.listFiles();
		if (files == null || files.length == 0) 
			return null;

		File lastModifiedFile = files[0];
		for (int i = 1; i < files.length; i++) 
		{
			if (lastModifiedFile.lastModified() < files[i].lastModified())
				lastModifiedFile = files[i];
		}
		return lastModifiedFile;
	}

	public  boolean isValidURL(String url)
	{
		try 
		{
			new URL(url).toURI();
			return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}

	//Functions used in API's:Update Product
	public FileWriter writeCSVFile(String fileName,int productid,LinkedHashMap<String, String> headerWithValues)
	{
		final String COMMA_DELIMITER = ",";
		final String NEW_LINE_SEPARATOR = "\n";

		FileWriter fileWriter=null;
		try
		{
			fileWriter=new FileWriter(fileName);
			fileWriter.append("Product Id").append(COMMA_DELIMITER);
			//Set Headers Values in CSV
			for(String header:headerWithValues.keySet())
				fileWriter.append(header).append(COMMA_DELIMITER);
			fileWriter.append(NEW_LINE_SEPARATOR);
			fileWriter.append(String.valueOf(productid)).append(COMMA_DELIMITER);
			//Set Values in CSV
			for(String header:headerWithValues.keySet())
				fileWriter.append(headerWithValues.get(header)).append(COMMA_DELIMITER);
			fileWriter.append(NEW_LINE_SEPARATOR);

			System.out.println("CSV file was created successfully !!!");
		}
		catch (Exception e)
		{
			System.out.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
		}
		finally 
		{
			try
			{
				fileWriter.flush();
				fileWriter.close();
			}
			catch (IOException e) 
			{
				System.out.println("Error while flushing/closing fileWriter !!!");
				e.printStackTrace();
			}
		}

		return fileWriter;

	}

	//Function Called in dissociateProductCSVAPI
	public FileWriter writeCSVFile(String fileName,HashMap<String, String> fetchProducts,ArrayList<String> headers,ArrayList<String> values)
	{
		final String COMMA_DELIMITER = ",";
		final String NEW_LINE_SEPARATOR = "\n";

		FileWriter fileWriter=null;
		try
		{
			fileWriter=new FileWriter(fileName);
			fileWriter.append("Product Id").append(COMMA_DELIMITER);

			//Set Headers Values in CSV
			for(int header=0;header<headers.size();header++)
			{
				fileWriter.append(headers.get(header));
				if(header<headers.size()-1)
					fileWriter.append(COMMA_DELIMITER);
			}

			fileWriter.append(NEW_LINE_SEPARATOR);

			//Set Values in CSV
			for(String productType:fetchProducts.keySet() )
				fileWriter.append(String.valueOf(fetchProducts.get(productType))).append(COMMA_DELIMITER).append(values.get(0)).append(NEW_LINE_SEPARATOR);

			System.out.println("CSV file was created successfully !!!");
		}
		catch (Exception e)
		{
			System.out.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
		}
		finally 
		{
			try
			{
				fileWriter.flush();
				fileWriter.close();
			}
			catch (IOException e) 
			{
				System.out.println("Error while flushing/closing fileWriter !!!");
				e.printStackTrace();
			}
		}

		return fileWriter;

	}

	public String getRepresentableStringFromMap(Map<? extends Object,? extends Object> map) {
		String formattedString = "";
		for(Object key : map.keySet()) {
			formattedString = formattedString+key.toString()+" -> "+map.get(key).toString()+"\n";
		}
		return formattedString;
	}

	public String getRepresentableStringFromList(List<? extends Object> list)  {
		String formattedString = "";
		for(Object item : list) {
			formattedString  = formattedString+ item.toString() +"\n";
		}
		return formattedString;
	}

	public Map<?,?> getDifferenceOfTwoMaps(Map<?,?> map1, Map<?,?> map2){
		Map<Object, Object> diffMap=null;
		diffMap = new HashMap<>();
		Set<?> keyset1 = map1.keySet();
		Set<?> keyset2 = map2.keySet();
		Set<Object> diffKeyset = new HashSet<>();
		for(Object key: keyset1) {
			if(keyset2.contains(key)) {
				if(!map1.get(key).equals(map2.get(key))) {
					diffMap.put(key, map1.get(key));
					diffKeyset.add(key);
				}
			}else {
				diffMap.put(key, map1.get(key));
				diffKeyset.add(key);
			}
		}


		for(Object key: keyset2) {
			if(!diffKeyset.contains(key) && !keyset1.contains(key)) {
				diffMap.put(key, map2.get(key));
			}
		}

		return diffMap;
	}

}
