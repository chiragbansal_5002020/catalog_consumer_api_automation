package com.paytm.utils;

import lombok.Data;

@Data
public class DBConnectionParamsPacket {
	private String sDatabaseIP;
	private int iDBPortNumber;
	private String sDatabaseName;
	private String sUserName;
	private String sPassword;
}
