package com.paytm.requests.initiliazers;

import io.restassured.specification.RequestSpecification;

public class RequestSpecProviderAccessToken implements RequestSpec{

	@Override
	public RequestSpecification getSpec() {
		RequestSpecification spec = RequestSpec.getGlobalRequestSpec();
		spec.queryParam("client_id", "merchant-integration");
		spec.queryParam("client_secret", "1f9009680f0b950d502b7b59e6eb3e312344d137");
		spec.queryParam("grant_type", "authorization_code");
		spec.baseUri("https://persona-staging.paytm.com/oauth2/token");
		spec.queryParam("state", "a1b2c3d4");
		return spec;
	}

}
