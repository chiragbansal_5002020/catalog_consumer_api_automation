package com.paytm.requests.initiliazers;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

public interface RequestSpec {
	
	public static RequestSpecification getGlobalRequestSpec() {
		RequestSpecBuilder builder = new RequestSpecBuilder();
		RequestSpecification spec = builder.build();
		return spec;
	}
	
	public RequestSpecification getSpec();
}
