package com.paytm.requests.initiliazers;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.methods.RequestBuilder;

import com.paytm.security.AuthTokenProvider;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

public class SpecProvider1 {
	
	private static RequestSpecBuilder reqBuilderSellerDev;
	private static RequestSpecBuilder reqBuilderAuthCodeRequest;
	private static RequestSpecBuilder reqBuilderAccessTokenRequest;
	private static RequestSpecBuilder reqBuilderGrid;
	
	public static RequestSpecification reqSpecSellerDev;
	public static RequestSpecification reqSpecGrid;
	public static RequestSpecification reqSpecAuthCode;
	public static RequestSpecification reqSpecAccessToken;
	
	 
	static {
		reqBuilderAuthCodeRequest = new RequestSpecBuilder();
		reqBuilderAuthCodeRequest.addParam("username", "chirag.bansal@paytmmall.com");
		reqBuilderAuthCodeRequest.addParam("password", "paytm@567");
		reqBuilderAuthCodeRequest.addParam("state", "a1b2c3d4");
		reqBuilderAuthCodeRequest.addParam("notredirect", "true");
		reqBuilderAuthCodeRequest.addParam("client_id","merchant-integration");
		reqBuilderAuthCodeRequest.addParam("response_type", "code");
		reqBuilderAuthCodeRequest.setBaseUri("https://persona-staging.paytm.com/oauth2/authorize");
		reqSpecAuthCode = reqBuilderAuthCodeRequest.build();
		
	}
	
	static {
		reqBuilderAccessTokenRequest = new RequestSpecBuilder();
		reqBuilderAccessTokenRequest.addParam("client_id", "merchant-integration");
		reqBuilderAccessTokenRequest.addParam("client_secret", "1f9009680f0b950d502b7b59e6eb3e312344d137");
		reqBuilderAccessTokenRequest.addParam("grant_type", "authorization_code");
		reqBuilderAccessTokenRequest.setBaseUri("https://persona-staging.paytm.com/oauth2/token");
		reqBuilderAccessTokenRequest.addParam("state", "a1b2c3d4");
		reqSpecAccessToken = reqBuilderAccessTokenRequest.build();
	}
	
	static {
		
		String access_token = AuthTokenProvider.getAccessToken();
		
		reqBuilderSellerDev = new RequestSpecBuilder();
		Map<String,String> headers = new HashMap<String,String>();
		headers.put("Origin", "https://seller-dev.paytm.com");
		headers.put("Accept-Encoding", "gzip, deflate, br");
		headers.put("Accept-Language","en-GB,en-US;q=0.9,en;q=0.8");
		headers.put("User-Agent","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36");
		headers.put("Content-Type","application/json");
		headers.put("Accept","application/json, text/plain, */*");
		headers.put("Referer","https://seller-dev.paytm.com/catalog/catalog_new");
		headers.put("Connection", "keep-alive");
		
		reqBuilderSellerDev.addHeaders(headers);
		reqBuilderSellerDev.addQueryParam("client", "web");
		reqBuilderSellerDev.addQueryParam("authtoken",access_token);
		
		reqSpecSellerDev = reqBuilderSellerDev.build();
	}
	
	static {
		reqBuilderGrid =  new RequestSpecBuilder();
		reqBuilderGrid.addParam("channel", "web");
		reqBuilderGrid.addParam("child_site_id", 6);
		reqBuilderGrid.addParam("site_id", 2);
		reqBuilderGrid.addParam("version", 2);
		reqBuilderGrid.addParam("items_per_page",32);
		reqSpecGrid = reqBuilderGrid.build();
	}
        
}
