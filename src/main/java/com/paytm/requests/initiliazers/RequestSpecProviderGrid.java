package com.paytm.requests.initiliazers;

import io.restassured.specification.RequestSpecification;

public class RequestSpecProviderGrid implements RequestSpec{

	@Override
	public RequestSpecification getSpec() {
		RequestSpecification spec  = RequestSpec.getGlobalRequestSpec();
		spec.queryParam("channel", "web");
		spec.queryParam("child_site_id", 6);
		spec.queryParam("site_id", 2);
		spec.queryParam("version", 2);
		//spec.queryParam("items_per_page",32);
		return spec;
	}

}
