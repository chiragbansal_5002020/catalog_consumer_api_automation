package com.paytm.requests.initiliazers;

import io.restassured.specification.RequestSpecification;

public class RequestSpecProviderAuthCode implements RequestSpec{

	@Override
	public RequestSpecification getSpec() {
		RequestSpecification spec = RequestSpec.getGlobalRequestSpec();
		spec.queryParam("username", "chirag.bansal@paytmmall.com");
		spec.queryParam("password", "paytm@567");
		spec.queryParam("state", "a1b2c3d4");
		spec.queryParam("notredirect", "true");
		spec.queryParam("client_id","merchant-integration");
		spec.queryParam("response_type", "code");
		spec.baseUri("https://persona-staging.paytm.com/oauth2/authorize");
		return spec;
	}

}
