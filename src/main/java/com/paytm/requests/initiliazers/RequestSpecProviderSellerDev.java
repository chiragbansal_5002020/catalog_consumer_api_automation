package com.paytm.requests.initiliazers;

import java.util.HashMap;
import java.util.Map;

import com.paytm.security.AuthTokenProvider;

import io.restassured.specification.RequestSpecification;

public class RequestSpecProviderSellerDev implements RequestSpec{

	@Override
	public RequestSpecification getSpec() {
		String access_token = AuthTokenProvider.getAccessToken();
		RequestSpecification spec = RequestSpec.getGlobalRequestSpec();
		Map<String,String> headers = new HashMap<String,String>();
		headers.put("Origin", "https://seller-dev.paytm.com");
		headers.put("Accept-Encoding", "gzip, deflate, br");
		headers.put("Accept-Language","en-GB,en-US;q=0.9,en;q=0.8");
		headers.put("User-Agent","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36");
		headers.put("Content-Type","application/json");
		headers.put("Accept","application/json, text/plain, */*");
		headers.put("Referer","https://seller-dev.paytm.com/catalog/catalog_new");
		headers.put("Connection", "keep-alive");
		
		spec.headers(headers);
		spec.queryParam("client", "web");
		spec.queryParam("authtoken",access_token);
		return null;
	}

}
