package com.paytm.requestbodyproviders;

import com.paytm.utils.GeneralUtils;

public class ProductJsonBodyProvider implements JsonBodyProvider {

	public String getJson(String... args) throws Exception {
		if(args.length==0) {
			throw new Exception("Category Id parameter missing in function call!");
		}
		
		String categoryId = args[0];
		String jsonString = GeneralUtils.readFileContent(
				"./src/main/resources/PostRequestData/product_creation_data.json");
		int randomNumber = GeneralUtils.randomNumberGenerator(10, 10000);
		int randomPrice = GeneralUtils.randomNumberGenerator(200, 1000);
		
		String newProductName = "Product_Test_"+ randomNumber;
		String newMRP = Integer.toString(randomPrice);
		String newPrice = Integer.toString(randomPrice - GeneralUtils.randomNumberGenerator(50, 100));
		
		System.out.println("New Product Name : "+ newProductName);
		System.out.println("New MRP Price : "+ newMRP);
		System.out.println("New Price : "+newPrice);
		System.out.println("Category ID for product : "+categoryId);
		
		jsonString = jsonString.replace("$category_id", categoryId)
			.replace("$product_name", newProductName)
			.replace("$mrp_price",newMRP)
			.replace("$price",newPrice);
		
		return jsonString;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println(new ProductJsonBodyProvider().getJson("11223"));
	}
	
}
