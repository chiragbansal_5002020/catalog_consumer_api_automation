package com.paytm.requestbodyproviders;

import java.io.IOException;

import com.paytm.utils.GeneralUtils;

public class CategoryJsonBodyProvider implements JsonBodyProvider{

	public String getJson(String... args) throws IOException {
		String jsonString = GeneralUtils.readFileContent(
				"./src/main/resources/PostRequestData/category_creation_data.json");
		int randomNumber = GeneralUtils.randomNumberGenerator(10, 10000);
		String newCategoryName = "Category_Test_"+ randomNumber;
		String newSeoSearchURL = "Search_Test_"+randomNumber;
		System.out.println("New Category Name : "+newCategoryName);
		System.out.println("New Seo Search URL : "+newSeoSearchURL);
		jsonString = jsonString.replace("$category_name", newCategoryName)
			.replace("$seo_url", newSeoSearchURL);
		return jsonString;
	}
	
	public static void main(String[] args) throws IOException {
		System.out.println(new CategoryJsonBodyProvider().getJson());
	}
	
}
