package com.paytm.requestbodyproviders;

public interface JsonBodyProvider {
	String getJson(String... args) throws Exception;
}
