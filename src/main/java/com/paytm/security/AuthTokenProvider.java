package com.paytm.security;

import static io.restassured.RestAssured.given;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.paytm.pojo.response.Access_Token;
import com.paytm.pojo.response.AuthCode;
import com.paytm.rest.requestresponse.AccesssTokenRequest;
import com.paytm.rest.requestresponse.AuthCodeRequest;

public class AuthTokenProvider {
	
	static String access_token ;
	
	public static String getAccessToken() {
		/*String code = given()
				.when()
				.get("https://persona-staging.paytm.com/oauth2/authorize?"
						+ "username=chirag.bansal@paytmmall.com"
						+ "&"
						+ "password=paytm@567"
						+ "&"
						+ "state=a1b2c3d4"
						+ "&"
						+ "notredirect=true"
						+ "&"
						+ "client_id=merchant-integration"
						+ "&"
						+ "response_type=code'")
				.then()
				.statusCode(200)
				.extract()
				.path("code");
		
		
		String access_token = given()
				.when()
				.get("https://persona-staging.paytm.com/oauth2/token?"
						+ "code"+"="+code
						+ "&"
						+ "client_id=merchant-integration"
						+ "&"
						+ "client_secret=1f9009680f0b950d502b7b59e6eb3e312344d137"
						+ "&"
						+ "grant_type=authorization_code"
						+ "&"
						+ "state=a1b2c3d4")
				.then()
				.statusCode(200)
				.extract()
				.path("access_token");
		*/
		
		if(access_token==null || access_token.isEmpty()) {
			//access_token = getTokenUsingAPI();
		}
		return access_token;
	}
	
	
	public static String getNewToken() {
		String token = getTokenUsingAPI();
		access_token = token;
		return access_token;
	}
	
	private static String getTokenUsingAPI() {
		String token = null;
		try {
			AuthCode code = (AuthCode) new AuthCodeRequest().call();
			Access_Token access_token_object = (Access_Token) new AccesssTokenRequest(code.getCode()).call();
			token = access_token_object.getAccess_token();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return token;
	}
	
	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException, InterruptedException {
		System.out.println(AuthTokenProvider.getAccessToken());
	}
}
