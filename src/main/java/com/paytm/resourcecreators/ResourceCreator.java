package com.paytm.resourcecreators;

import static io.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.List;

import com.paytm.requestbodyproviders.CategoryJsonBodyProvider;
import com.paytm.requestbodyproviders.ProductJsonBodyProvider;
import com.paytm.requests.initiliazers.RequestSpecProviderSellerDev;
import com.paytm.rest.requestresponse.GeneralRequest;
import com.paytm.rest.requestresponse.RequestType;
import com.paytm.security.AuthTokenProvider;

import io.restassured.response.Response;

public class ResourceCreator {

	public static int createNewCategory() {
		Response response = null;
		int newCategoryID = 0;
		try {
			String jsonBody = new CategoryJsonBodyProvider().getJson();
			System.out.println(jsonBody);
			String access_token = AuthTokenProvider.getAccessToken();
			GeneralRequest request = new GeneralRequest(RequestType.POST, 
					"https://catalogadmin-staging.paytm.com/dev1/v1/category");
			request.setQueryParameter("client", "web");
			request.setQueryParameter("authtoken", access_token);
			request.setFormParameter("vertical_id", 2);
			request.setFormParameter("data", jsonBody);
			response = request.getResponse();
			
			/*response = given().log().all()
					.queryParams("client", "web")
					.queryParam("authtoken", access_token)
					.formParam("vertical_id", 2)
					.formParam("data", jsonBody)
					.when()
					.post("https://catalogadmin-staging.paytm.com/dev1/v1/category")
					.then()
					.extract()
					.response();*/

			if(response.getStatusCode()!=200) {
				throw new Exception("Category not created. Following is the response: "+"\n"+
						response.asString());

			}

			newCategoryID = (Integer)response.path("id");
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}

		System.out.println("New Category ID - "+newCategoryID);
		return newCategoryID;
	}

	public static List<Integer> createNewProduct(int categoryId, int count){
		Response response = null;
		List<Integer> newProductIDs = new ArrayList<>();
		try {
			while(count-->0) {
				List<Integer> tempList = new ArrayList<>();
				String jsonBody = new ProductJsonBodyProvider().getJson(Integer.toString(categoryId));
				System.out.println(jsonBody);
				response = given()
						.spec(new RequestSpecProviderSellerDev().getSpec())
						.body(jsonBody)
						.when()
						.post("https://catalogadmin-staging.paytm.com/dev1/v1/merchant/600721/create/each/product.json")
						.then()
						.extract()
						.response();

				if(response.getStatusCode()!=200) {
					throw new Exception("Product not created. Following is the response: "+"\n"+
							response.asString());

				}
				System.out.println(response.asString());

				tempList = response.path("res.product_ids");
				newProductIDs.add(tempList.get(0));
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return newProductIDs;
	}
}
