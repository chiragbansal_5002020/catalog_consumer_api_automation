package com.paytm.pojo.response;

import io.restassured.response.Response;
import lombok.Data;

@Data
public class GeneralResponse implements RestResponse{
	Response restResponse;
}
