package com.paytm.pojo.response;

import lombok.Data;

@Data
public class Price implements RestResponse{
	String selling;
}
