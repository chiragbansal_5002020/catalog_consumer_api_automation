package com.paytm.pojo.response;

import lombok.Data;

@Data
public class AuthCode implements RestResponse{
	String code;
}
