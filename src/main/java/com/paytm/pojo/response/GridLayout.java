package com.paytm.pojo.response;

import lombok.Data;

@Data
public class GridLayout implements RestResponse{
	int discoverability;
	double actual_price;
	int add_to_cart;
	int category_id;
	double offer_price;
	int product_id;
	String image_url;
	String url;
	String seourl;
	String newurl;
}
