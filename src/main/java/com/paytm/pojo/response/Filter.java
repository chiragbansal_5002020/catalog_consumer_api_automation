package com.paytm.pojo.response;

import java.util.List;

import lombok.Data;

@Data
public class Filter {
	String title;
	String type;
	String filter_param;
	List<FilterValue> values;
}
