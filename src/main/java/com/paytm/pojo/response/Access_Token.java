package com.paytm.pojo.response;

import lombok.Data;

@Data
public class Access_Token implements RestResponse{
	String access_token;
}
