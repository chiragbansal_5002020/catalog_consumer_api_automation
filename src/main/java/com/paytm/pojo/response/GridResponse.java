package com.paytm.pojo.response;

import java.util.List;

import lombok.Data;

@Data
public class GridResponse implements RestResponse {
	
	List<GridLayout> grid_layout;
	int grid_type;
	int add_to_cart;
	int totalCount;
	List<Filter> filters;
	int serviceability_enabled;
}
