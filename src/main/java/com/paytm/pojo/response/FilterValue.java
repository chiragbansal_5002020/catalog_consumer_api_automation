package com.paytm.pojo.response;

import lombok.Data;

@Data
public class FilterValue {
	String id;
	String name;
	String count;
	String min;
	String max;
}
