package com.paytm.pojo.response;

import lombok.Data;

@Data
public class Product implements RestResponse{
	Price price;
}
