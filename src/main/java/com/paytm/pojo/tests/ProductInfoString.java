package com.paytm.pojo.tests;

import lombok.Data;

@Data
public class ProductInfoString {
	String standalone;
	int[] discoverability;
	String created_by;
	String email;
	int mop;
	String dimensions;
	int multi_shipment;
	int productStandardName;
}
