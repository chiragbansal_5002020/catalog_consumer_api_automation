package com.paytm.pojo.tests;

import lombok.Data;

@Data
public class CategoryInfoString {
	String urltype;
	String sequential_filter;
	String hsn;
	int serviceability_enabled;
	Actions actions;
}
