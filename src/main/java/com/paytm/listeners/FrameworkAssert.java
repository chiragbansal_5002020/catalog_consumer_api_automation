package com.paytm.listeners;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.testng.Assert;
import org.testng.Reporter;

public class FrameworkAssert{
	
	private static void logCollection(Collection<?> actual, Collection<?> expected, String... msg) {
		String exp = "";
		String act = "";
		Iterator<?> it = expected.iterator();
		while(it.hasNext()) {
			exp+=it.next().toString()+"\n";
		}
		it = actual.iterator();
		while(it.hasNext()) {
			act+=it.next().toString()+"\n";
		}
		String message="";
		if(msg.length>0) {
			for(int i=0; i<msg.length; i++)
				message+=msg[i];
			
		}
		Reporter.log("Expected : "+exp+"\n"
				+"Actual : "+act+"\n"
				+message);
	}
	
	private static void logObjectArray(Object[] actual, Object[] expected, String... msg) {
		String exp = "";
		String act = "";
		for(int i=0; i<expected.length; i++)
			exp+=expected[i].toString()+"\n";
		
		for(int i=0; i<actual.length; i++)
			act+=actual[i].toString()+"\n";
		
		String message="";
		if(msg.length>0) {
			for(int i=0; i<msg.length; i++)
				message+=msg[i];
			
		}
		Reporter.log("Expected : "+exp+"\n"
				+"Actual : "+act+"\n"
				+message);
	}
	
	private static void logMap(Map<?,?> actual, Map<?,?> expected, String... msg) {
		
		String exp = "";
		String act = "";
		
		for(Object key : expected.keySet()) {
			exp+=key+" -> "+expected.get(key).toString()+"\n";
		}
		
		for(Object key : actual.keySet()) {
			act+=key+" -> "+actual.get(key).toString()+"\n";
		}
		
		String message="";
		if(msg.length>0) {
			for(int i=0; i<msg.length; i++)
				message+=msg[i];
			
		}
		Reporter.log("Expected : "+exp+"\n"
				+"Actual : "+act+"\n"
				+message);
	}
	
	public static void assertEquals(Object actual, Object expected, String message) {
		Reporter.log("Expected: "+expected.toString()+"\n"
				+ "Actual: "+actual.toString()+"\n"
				+message);
		Assert.assertEquals(actual, expected,message);
	}

	public static void assertTrue(boolean condition, String message) {
		Reporter.log("Expected the condition to be true"+"\n"
				+message);  
		Assert.assertTrue(condition, message);
	}

	public static void assertFalse(boolean condition, String message) {
		Reporter.log("Expected the condition to be false"+"\n"+
						message);
		Assert.assertFalse(condition,message);
	}


	public static void assertEquals(byte[] actual, byte[] expected) {
		Reporter.log("Expected Byte Array : "+expected.toString()+"\n"+
					"Actual Byte Array : "+actual.toString());
		Assert.assertEquals(actual, expected);
	}

	public static void assertEquals(byte[] actual, byte[] expected, String message) {
		Reporter.log("Expected Byte Array : "+expected.toString()+"\n"+
				"Actual Byte Array : "+actual.toString()+"\n"+
				"Message - "+message);
		Assert.assertEquals(actual, expected,message);
	}

	public static void assertEquals(short[] actual, short[] expected) {
		Reporter.log("Expected : "+expected+"\n"+
					 "Actual : "+actual);
		Assert.assertEquals(actual, expected);
	}

	public static void assertEquals(short[] actual, short[] expected, String message) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual+"\n"
				 +message);
		Assert.assertEquals(actual, expected,message);
	}

	public static void assertEquals(int[] actual, int[] expected) {
		Reporter.log("Expected : "+expected+"\n"+
					"Actual : "+actual);
		Assert.assertEquals(actual, expected, "");
	}

	public static void assertEquals(int[] actual, int[] expected, String message) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual+"\n"
				 +message);
		Assert.assertEquals(actual, expected,message);
	}

	public static void assertEquals(boolean[] actual, boolean[] expected) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual);
		Assert.assertEquals(actual, expected);
	}

	public static void assertEquals(boolean[] actual, boolean[] expected, String message) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual+"\n"
				 +message);
		Assert.assertEquals(actual, expected,message);
	}

	public static void assertEquals(char[] actual, char[] expected) {
		Reporter.log("Expected : "+expected.toString()+"\n"+
				 "Actual : "+actual.toString());
		Assert.assertEquals(actual, expected);
	}

	
	public static void assertEquals(char[] actual, char[] expected, String message) {
		Reporter.log("Expected : "+expected.toString()+"\n"+
				 "Actual : "+actual.toString()+"\n"
				 +message);
		Assert.assertEquals(actual, expected,message);
	}

	public static void assertEquals(float[] actual, float[] expected) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual);
		Assert.assertEquals(actual, expected);
	}

	public static void assertEquals(float[] actual, float[] expected, String message) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual+"\n"
				 +message);
		Assert.assertEquals(actual, expected,message);
	}

	  
	public static void assertEquals(double[] actual, double[] expected) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual);
		Assert.assertEquals(actual, expected);
	}

	public static void assertEquals(double[] actual, double[] expected, String message) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual+"\n"
				 +message);
		Assert.assertEquals(actual, expected,message);
	}

	public static void assertEquals(long[] actual, long[] expected) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual);
		Assert.assertEquals(actual, expected);
	}

	public static void assertEquals(long[] actual, long[] expected, String message) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual+"\n"
				 +message);
		Assert.assertEquals(actual, expected,message);
	}

	
	public static void assertEquals(Object actual, Object expected) {
		Reporter.log("Expected : "+expected.toString()+"\n"+
				 "Actual : "+actual.toString());
		Assert.assertEquals(actual, expected);
	}

	public static void assertEquals(String actual, String expected, String message) {
		
		Reporter.log("Expected : "+expected+"\n"
				+"Actual : "+actual+"\n"
				 +message);
		Assert.assertEquals(actual, expected, message);
	}

	public static void assertEquals(String actual, String expected) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual);
		Assert.assertEquals(actual, expected);
	}

	
	public static void assertEquals(long actual, long expected, String message) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual+"\n"
				 +message);
		Assert.assertEquals(actual, expected, message);
	}

	public static void assertEquals(long actual, long expected) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual);
		Assert.assertEquals(actual, expected);
	}

	public static void assertEquals(boolean actual, boolean expected, String message) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual+"\n"
				 +message);
		Assert.assertEquals(actual, expected, message);
	}

	public static void assertEquals(boolean actual, boolean expected) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual);
		Assert.assertEquals(actual, expected);
	}

	public static void assertEquals(byte actual, byte expected, String message) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual+"\n"
				 +message);
		Assert.assertEquals(actual, expected, message);
	}

	
	public static void assertEquals(byte actual, byte expected) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual);
		Assert.assertEquals(actual, expected);
	}

	public static void assertEquals(char actual, char expected, String message) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual+"\n"
				 +message);
		Assert.assertEquals(actual, expected, message);
	}

	public static void assertEquals(char actual, char expected) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual);
		Assert.assertEquals(actual, expected);
	}

	public static void assertEquals(short actual, short expected, String message) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual+"\n"
				 +message);
		Assert.assertEquals(actual, expected, message);
	}

	public static void assertEquals(short actual, short expected) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual);
		Assert.assertEquals(actual, expected);
	}

	public static void assertEquals(int actual,  int expected, String message) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual+"\n"
				 +message);
		Assert.assertEquals(actual, expected, message); 
	}

	public static void assertEquals(int actual, int expected) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual);
		Assert.assertEquals(actual, expected);
	}

	public static void assertEquals(Collection<?> actual, Collection<?> expected) {
		logCollection(actual,expected);
		Assert.assertEquals(actual, expected);
	}

	public static void assertEquals(Collection<?> actual, Collection<?> expected, String message) {
		logCollection(actual,expected,message);
		Assert.assertEquals(actual, expected, message);
	}

	public static void assertEquals(Iterator<?> actual, Iterator<?> expected) {
		String exp = "";
		String act = "";
		while(expected.hasNext()) {
			exp+=expected.next().toString()+"\n";
		}
		
		while(actual.hasNext()) {
			act+=actual.next().toString()+"\n";
		}
		Reporter.log("Expected : "+exp+"\n"
				+"Actual : "+act);
		
		Assert.assertEquals(actual,expected);
	}

	public static void assertEquals(Iterator<?> actual, Iterator<?> expected, String message) {
		String exp = "";
		String act = "";
		while(expected.hasNext()) {
			exp+=expected.next().toString()+"\n";
		}
		
		while(actual.hasNext()) {
			act+=actual.next().toString()+"\n";
		}
		Reporter.log("Expected : "+exp+"\n"
				+"Actual : "+act+"\n"
				+message);
		
		Assert.assertEquals(actual,expected,message);

	}


	public static void assertEquals(Object[] actual, Object[] expected, String message) {
		logObjectArray(actual, expected,message);
		Assert.assertEquals(actual, expected,message);
	}


	public static void assertEqualsNoOrder(Object[] actual, Object[] expected, String message) {
		logObjectArray(actual, expected,message);
		Assert.assertEqualsNoOrder(actual, expected, message);
	}

	public static void assertEquals(Object[] actual, Object[] expected) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual);
		Assert.assertEquals(actual, expected); 
	}

	public static void assertEqualsNoOrder(Object[] actual, Object[] expected) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual);
		Assert.assertEqualsNoOrder(actual, expected); 
	}


	public static void assertEquals(Set<?> actual, Set<?> expected) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual);
		Assert.assertEquals(actual, expected); 
	}

	public static void assertEquals(Set<?> actual, Set<?> expected, String message) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual+"\n"
				 +message);
		Assert.assertEquals(actual, expected, message); 
	}

	public static void assertEqualsDeep(Set<?> actual, Set<?> expected, String message) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual+"\n"
				 +message);
		Assert.assertEqualsDeep(actual, expected, message); 
	}
 
	public static void assertEquals(Map<?, ?> actual, Map<?, ?> expected, String message) {
		Reporter.log("Expected : "+expected+"\n"+
				 "Actual : "+actual+"\n"
				 +message);
		Assert.assertEquals(actual, expected, message); 

	}

	public static void assertEqualsDeep(Map<?, ?> actual, Map<?, ?> expected) {
		logMap(actual, expected);
		Assert.assertEqualsDeep(actual, expected);
	}


	public static void assertEqualsDeep(Map<?, ?> actual, Map<?, ?> expected, String message) {
		logMap(actual, expected, message);
		Assert.assertEqualsDeep(actual, expected,message);

	}

	/////
	// assertNotEquals
	////

	public static void assertNotEquals(Object actual1, Object actual2, String message) {
		Reporter.log("Asserting not equal"+
				"First parameter  : "+actual1+"\n"+
				"Second parameter : "+actual2+"\n"
				+message);
		Assert.assertNotEquals(actual1, actual2, message); 
	}

	public static void assertNotEquals(Object actual1, Object actual2) {
		Reporter.log("Asserting not equal"+
				"First parameter  : "+actual1+"\n"+
				"Second parameter : "+actual2);
		Assert.assertNotEquals(actual1, actual2); 
	}

	static void assertNotEquals(String actual1, String actual2, String message) {
		Reporter.log("Asserting not equal"+
				"First parameter  : "+actual1+"\n"+
				"Second parameter : "+actual2+"\n"
				+message);
		Assert.assertNotEquals(actual1, actual2, message); 
	}

	static void assertNotEquals(String actual1, String actual2) {
		Reporter.log("Asserting not equal"+
				"First parameter  : "+actual1+"\n"+
				"Second parameter : "+actual2);
		Assert.assertNotEquals(actual1, actual2); 
	}

	static void assertNotEquals(long actual1, long actual2, String message) {
		Reporter.log("Asserting not equal"+
				"First parameter  : "+actual1+"\n"+
				"Second parameter : "+actual2+"\n"
				+message);
		Assert.assertNotEquals(actual1, actual2, message); 
	}

	static void assertNotEquals(long actual1, long actual2) {
		Reporter.log("Asserting not equal"+
				"First parameter  : "+actual1+"\n"+
				"Second parameter : "+actual2);
		Assert.assertNotEquals(actual1, actual2); 
	}

	static void assertNotEquals(boolean actual1, boolean actual2, String message) {
		Reporter.log("Asserting not equal"+
				"First parameter  : "+actual1+"\n"+
				"Second parameter : "+actual2+"\n"
				+message);
		Assert.assertNotEquals(actual1, actual2, message); 
	}

	static void assertNotEquals(boolean actual1, boolean actual2) {
		Reporter.log("Asserting not equal"+
				"First parameter  : "+actual1+"\n"+
				"Second parameter : "+actual2);
		Assert.assertNotEquals(actual1, actual2); 
	}

	static void assertNotEquals(byte actual1, byte actual2, String message) {
		Reporter.log("Asserting not equal"+
				"First parameter  : "+actual1+"\n"+
				"Second parameter : "+actual2+"\n"
				+message);
		Assert.assertNotEquals(actual1, actual2, message); 
	}

	static void assertNotEquals(byte actual1, byte actual2) {
		Reporter.log("Asserting not equal"+
				"First parameter  : "+actual1+"\n"+
				"Second parameter : "+actual2);
		Assert.assertNotEquals(actual1, actual2); 
	}

	static void assertNotEquals(char actual1, char actual2, String message) {
		Reporter.log("Asserting not equal"+
				"First parameter  : "+actual1+"\n"+
				"Second parameter : "+actual2+"\n"
				+message);
		Assert.assertNotEquals(actual1, actual2, message); 
	}

	static void assertNotEquals(char actual1, char actual2) {
		Reporter.log("Asserting not equal"+
				"First parameter  : "+actual1+"\n"+
				"Second parameter : "+actual2);
		Assert.assertNotEquals(actual1, actual2); 
	}

	static void assertNotEquals(short actual1, short actual2, String message) {
		Reporter.log("Asserting not equal"+
				"First parameter  : "+actual1+"\n"+
				"Second parameter : "+actual2+"\n"
				+message);
		Assert.assertNotEquals(actual1, actual2, message); 
	}

	static void assertNotEquals(short actual1, short actual2) {
		Reporter.log("Asserting not equal"+
				"First parameter  : "+actual1+"\n"+
				"Second parameter : "+actual2);
		Assert.assertNotEquals(actual1, actual2); 
	}

	static void assertNotEquals(int actual1, int actual2, String message) {
		Reporter.log("Asserting not equal"+
				"First parameter  : "+actual1+"\n"+
				"Second parameter : "+actual2+"\n"
				+message);
		Assert.assertNotEquals(actual1, actual2, message); 
	}

	static void assertNotEquals(int actual1, int actual2) {
		Reporter.log("Asserting not equal"+
				"First parameter  : "+actual1+"\n"+
				"Second parameter : "+actual2);
		Assert.assertNotEquals(actual1, actual2); 
	}


	public static void assertNotEquals(Set<?> actual, Set<?> expected) {
		logCollection(actual, expected);
		Assert.assertNotEquals(actual, expected);
	}

	public static void assertNotEquals(Set<?> actual, Set<?> expected, String message) {
		logCollection(actual, expected,message);
		Assert.assertNotEquals(actual, expected,message);
	}

	public static void assertNotEqualsDeep(Set<?> actual, Set<?> expected) {
		logCollection(actual, expected);
		Assert.assertNotEqualsDeep(actual, expected);
	}

	public static void assertNotEqualsDeep(Set<?> actual, Set<?> expected, String message) {
		logCollection(actual, expected,message);
		Assert.assertNotEqualsDeep(actual, expected,message);
	}

	public static void assertNotEquals(Map<?, ?> actual, Map<?, ?> expected) {
		logMap(actual, expected);
		Assert.assertNotEquals(actual, expected);
	}

	public static void assertNotEquals(Map<?, ?> actual, Map<?, ?> expected, String message) {
		logMap(actual, expected,message);
		Assert.assertNotEquals(actual, expected,message);
	}

	public static void assertNotEqualsDeep(Map<?, ?> actual, Map<?, ?> expected) {
		logMap(actual, expected);
		Assert.assertNotEqualsDeep(actual, expected);
	}

	public static void assertNotEqualsDeep(Map<?, ?> actual, Map<?, ?> expected, String message) {
		logMap(actual, expected,message);
		Assert.assertNotEqualsDeep(actual, expected,message);
	}
}
