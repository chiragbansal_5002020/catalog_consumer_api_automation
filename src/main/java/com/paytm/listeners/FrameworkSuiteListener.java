package com.paytm.listeners;

import java.util.Map;
import java.util.Set;

import org.testng.IResultMap;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.ITestResult;

public class FrameworkSuiteListener implements ISuiteListener{

	@Override
	public void onStart(ISuite suite) {
		
	}

	@Override
	public void onFinish(ISuite suite) {
		Map<String, ISuiteResult> resultMap = suite.getResults();
		for(String test : resultMap.keySet()) {
			ISuiteResult result = resultMap.get(test);
			ITestContext context = result.getTestContext();
			IResultMap failedTestsResultMap = context.getFailedTests();
			IResultMap passedTestsResultMap = context.getPassedTests();
			IResultMap skippedTestsResultmap = context.getSkippedTests();
			
			Set<ITestResult> resultSet = failedTestsResultMap.getAllResults();
			for(ITestResult rs : resultSet) {
				System.out.println(rs.getTestName());
				System.out.println(rs.getStatus());
			}
		}
		
	}

}
