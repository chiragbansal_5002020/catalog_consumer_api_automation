package com.paytm.listeners;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.testng.IReporter;
import org.testng.IResultMap;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.CodeLanguage;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class CustomReporter implements IReporter {

	private static final String OUTPUT_FOLDER = "./";
	private static final String FILE_NAME = "index.html";

	private ExtentReports extent;

	@SuppressWarnings("rawtypes")
	@Override
	public void generateReport(List xmlSuites, List suites, String outputDirectory) {
		init();
		for (String s : Reporter.getOutput()) {
			extent.setTestRunnerOutput(s);
		}

		
		for (Object suite : suites.toArray()) {
			
			
			Map result = ((ISuite) suite).getResults();
			int testNumber=0;
			for (Object r : result.values()) {
				ITestContext context = ((ISuiteResult) r).getTestContext();
				buildTestNodes(context.getFailedTests(), Status.FAIL, testNumber);
				buildTestNodes(context.getSkippedTests(), Status.SKIP, testNumber);
				buildTestNodes(context.getPassedTests(), Status.PASS, testNumber);
				testNumber++;
			}
		}

		extent.flush();
	}

	private void init() {
		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(OUTPUT_FOLDER + FILE_NAME);
		htmlReporter.config().setTheme(Theme.STANDARD);
		htmlReporter.config().setTimeStampFormat("dd/MM/yyyy hh:mm:ss a");

		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		extent.setReportUsesManualConfiguration(true);
		extent.flush();
	}

	private void buildTestNodes(IResultMap tests, Status status, int testNumber) {
		ExtentTest test;

		if (tests.size() > 0) {
			for (ITestResult result : tests.getAllResults()) {
				
				test = extent.createTest(result.getMethod().getMethodName());

				for (String group : result.getMethod().getGroups())
					test.assignCategory(group);
				
				List<String> logs = Reporter.getOutput(result);
				for(String log : logs) {
					test.log(Status.INFO, "<pre>"+log+"</pre>");
				}
				
				if (result.getThrowable() != null) {
					test.log(status, result.getThrowable());
				} else {
					test.log(status, "Test " + status.toString().toLowerCase() + "ed");
				}
				
				test.getModel().setStartTime(getTime(result.getStartMillis()));
				test.getModel().setEndTime(getTime(result.getEndMillis()));
				test.getModel().getLogContext().get(testNumber).setTimestamp(getTime(result.getEndMillis()));
				extent.flush();
			}		
		}
	}

	private Date getTime(long millis) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(millis);
		return calendar.getTime();
	}
}