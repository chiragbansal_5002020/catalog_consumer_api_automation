package com.paytm.rest.requestresponse;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.paytm.pojo.response.Access_Token;
import com.paytm.pojo.response.RestResponse;
import com.paytm.requests.initiliazers.RequestSpecProviderAccessToken;

public class AccesssTokenRequest extends BaseRestRequest{
	String authCode;
	public AccesssTokenRequest(String authCode) {
		super(new RequestSpecProviderAccessToken().getSpec());
		this.authCode = authCode;
	}

	@Override
	public RestResponse call() throws JsonParseException, JsonMappingException, IOException {
		getRequestSpec().queryParam("code", authCode);
		return sendRequestAndGetResponse(RequestType.GET, Access_Token.class);
	}
	
}
