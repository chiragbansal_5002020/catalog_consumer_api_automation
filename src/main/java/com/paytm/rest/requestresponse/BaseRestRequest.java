package com.paytm.rest.requestresponse;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.paytm.pojo.response.RestResponse;
import com.paytm.utils.JsonHandler;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.Header;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public abstract class BaseRestRequest {
	RequestSpecification requestSpec;
	String resourceURL;
	Response responseFromRestRequest;
	
	public BaseRestRequest() {
		requestSpec = new RequestSpecBuilder().build();
		this.resourceURL="";
	}
	
	public BaseRestRequest(String completeURL) {
		requestSpec = new RequestSpecBuilder().build();
		this.resourceURL=completeURL;
	}
	
	public BaseRestRequest(RequestSpecification requestSpec, String resourceURL) {
		this.requestSpec = requestSpec;
		if(!resourceURL.startsWith("/"))
			this.resourceURL="/"+resourceURL;
		else
			this.resourceURL = resourceURL;
	}
	
	public BaseRestRequest(RequestSpecification requestSpec) {
		this.requestSpec = requestSpec;
		this.resourceURL = "";
	}

	public BaseRestRequest setQueryParameter(String key, String value) {
		this.requestSpec.queryParam(key, value);
		return this;
	}
	
	public BaseRestRequest setHeader(String name, String value) {
		this.requestSpec.header(new Header(name, value));
		return this;
	}
	
	public BaseRestRequest setBody(String jsonBody) {
		this.requestSpec.body(jsonBody);
		return this;
	}
	
	public BaseRestRequest setFormParameter(String key, Object value) {
		this.requestSpec.formParam(key, value);
		return this;
	}
	
	public RequestSpecification getRequestSpec() {
		return this.requestSpec;
	}
	
	protected RestResponse sendRequestAndGetResponse(RequestType type,Class<?> responseClass) throws JsonParseException, JsonMappingException, IOException{
		Response response = GenericRestRequestProcessor.getResponse(
				requestSpec, type, this.resourceURL);
		this.responseFromRestRequest = response;
		return JsonHandler.getObjectFromResponse(response, responseClass);
	}
	
	
	protected RestResponse sendRequestAndGetResponse(RequestType type, String resourceURL, Class<?> responseClass) throws JsonParseException, JsonMappingException, IOException{
		Response response = GenericRestRequestProcessor.getResponse(
				requestSpec, type, resourceURL);
		this.responseFromRestRequest = response;
		return JsonHandler.getObjectFromResponse(response, responseClass);
	}
	
	protected Response sendRequestAndGetResponse(RequestType type) {
		Response response = GenericRestRequestProcessor.getResponse(
				requestSpec, type, this.resourceURL);
		this.responseFromRestRequest = response;
		return response;
	}  
	
	public int getStatus() throws JsonParseException, JsonMappingException, IOException {
		if(responseFromRestRequest==null) {
			call();
		}
		return responseFromRestRequest.getStatusCode();
	}
	
	public Response getRestResponse() throws JsonParseException, JsonMappingException, IOException {
		if(responseFromRestRequest==null) {
			call();
		}
		return responseFromRestRequest;
	}
	
	public abstract RestResponse call() throws JsonParseException, JsonMappingException, IOException;
}
