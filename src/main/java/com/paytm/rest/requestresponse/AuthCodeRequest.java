package com.paytm.rest.requestresponse;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.paytm.pojo.response.AuthCode;
import com.paytm.pojo.response.RestResponse;
import com.paytm.requests.initiliazers.RequestSpecProviderAuthCode;

public class AuthCodeRequest extends BaseRestRequest{

	public AuthCodeRequest() {
		super(new RequestSpecProviderAuthCode().getSpec());
	}

	@Override
	public RestResponse call() throws JsonParseException, JsonMappingException, IOException {
		return sendRequestAndGetResponse(RequestType.GET, AuthCode.class);
	}
	
	
}
