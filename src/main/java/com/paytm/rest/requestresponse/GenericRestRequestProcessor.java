package com.paytm.rest.requestresponse;

import static io.restassured.RestAssured.config;
import static io.restassured.RestAssured.given;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.testng.Reporter;

import io.restassured.RestAssured;
import io.restassured.config.LogConfig;
import io.restassured.config.ParamConfig;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class GenericRestRequestProcessor {

	public static final ByteArrayOutputStream baos = new ByteArrayOutputStream();
	public static final PrintStream ps = new PrintStream(baos);
	
	static {
		RestAssured.config = config().logConfig(new LogConfig().defaultStream(ps));
	}
	
	public static Response getResponse(
			RequestSpecification requestSpec,
			RequestType type,
			String resourceURL) {
		
//		requestSpec.filter(
//				(req, response, ctx) -> {
//					Reporter.log(req.getBasePath());
//					return ctx.next(req, response); 
//					}
//				);
		
		Response response = null;

		if(type==RequestType.GET) {
			response =  given().log().all()
					.config(config().paramConfig(new ParamConfig().queryParamsUpdateStrategy(
							ParamConfig.UpdateStrategy.REPLACE)))
					.spec(requestSpec)
					.when()
					.get(resourceURL)
					.then()
					.extract().response();
		}
		else if(type==RequestType.POST) {
			response =  given().log().all()
					.config(config().paramConfig(new ParamConfig().queryParamsUpdateStrategy(
							ParamConfig.UpdateStrategy.REPLACE)))
					.spec(requestSpec)
					.when()
					.post(resourceURL)
					.then()
					.extract().response();
		}else if(type==RequestType.PUT) {
			response =  given().log().all()
					.config(config().paramConfig(new ParamConfig().queryParamsUpdateStrategy(
							ParamConfig.UpdateStrategy.REPLACE)))
					.spec(requestSpec)
					.when()
					.put(resourceURL)
					.then()
					.extract().response();
		}else if(type==RequestType.DELETE) {
			response =  given().log().all()
					.config(config().paramConfig(new ParamConfig().queryParamsUpdateStrategy(
							ParamConfig.UpdateStrategy.REPLACE)))
					.spec(requestSpec)
					.when()
					.delete(resourceURL)
					.then()
					.extract().response();
		}
		
		
		String responseStr = response.asString();
		
		String toLog = "<font color='green' face='verdana' size='2' style=\"font-weight:bold\">REST REQUEST</font> \n"+
						baos.toString()+"\n\n\n"
						+"<font color='"+getResponseFontColor(response.getStatusCode())+"'"
						+"face='verdana'"
						+"size='2'"
						+"style=\"font-weight:bold\">"
						+"RESPONSE ("+response.getStatusCode()+")"
						+"</font> \n"
						+"<textarea disabled> "
						+responseStr
						+"</textarea>";
					
		Reporter.log(toLog);
		baos.reset();
		return response;
		
	}
	
	private static String getResponseFontColor(int responseStatusCode) {
		String responseFontColor="";
		switch(responseStatusCode) {
		case 200:
			responseFontColor="green";
			break;
		case 404:
			responseFontColor="red";
			break;
		default:
			responseFontColor="blue";
		}
		
		return responseFontColor;
	}
}
