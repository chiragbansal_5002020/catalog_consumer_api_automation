package com.paytm.rest.requestresponse;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.paytm.pojo.response.GridResponse;
import com.paytm.pojo.response.RestResponse;
import com.paytm.requests.initiliazers.RequestSpecProviderGrid;


public class GridRequest extends BaseRestRequest {
	
	public GridRequest(String resourceURL) {
		super(new RequestSpecProviderGrid().getSpec(),resourceURL);
	}

	@Override
	public RestResponse call() throws JsonParseException, JsonMappingException, IOException {
		return sendRequestAndGetResponse(RequestType.GET, GridResponse.class);
	}

}
