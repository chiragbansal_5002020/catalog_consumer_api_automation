package com.paytm.rest.requestresponse;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.paytm.pojo.response.GeneralResponse;
import com.paytm.pojo.response.RestResponse;

import io.restassured.response.Response;

public class GeneralRequest extends BaseRestRequest{

	RequestType type;
	
	public GeneralRequest(RequestType type, String completeURL) {
		super(completeURL);
		this.type = type;
	}

	@Override
	public RestResponse call() throws JsonParseException, JsonMappingException, IOException {
		GeneralResponse generalResponseObject = new GeneralResponse();
		generalResponseObject.setRestResponse(sendRequestAndGetResponse(type));
		return generalResponseObject;
	}
	
	public Response getResponse() throws JsonParseException, JsonMappingException, IOException {
		GeneralResponse response = (GeneralResponse)call();
		return response.getRestResponse();
	}

}
